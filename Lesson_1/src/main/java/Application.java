import java.io.*;
import java.util.*;

public class Application {

    public static void start(String path) {
        System.out.println("Привет");
        openFile(path);
        System.out.println("Пока");
    }

    private static void openFile (String path) {
        List <String> lines = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
                        String line = null;
            while ((line = reader.readLine()) != null) {
                    lines.add(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        run(lines);
    }

    private static void run(List<String> lines) {
        deleteEmptyLine(lines);
        deleteDuplicate(lines);
        List <String> lines1 = new ArrayList<>();
        List <String> lines2 = new ArrayList<>();
        for (String string : lines) {
            if (string.startsWith("-")) {
                lines1.add(string);
            }else {
                lines2.add(string);
            }
        }
        textWriter(lines1);
        textWriter(lines2);
    }

    private static void deleteEmptyLine (List<String> strings) {
        strings.removeAll(Collections.singleton(null));
        strings.removeAll(Collections.singleton(""));
    }

    private static void deleteDuplicate (List<String> strings) {
        Collections.sort(strings);
        Object last = null;
        Iterator iterator = strings.iterator();
        while (iterator.hasNext()){
            Object object = iterator.next();
            if (object.equals(last)) {
                iterator.remove();
            }else {
                last = object;
            }
        }
    }

    private static void textWriter (List<String> strings) {
        int a = strings.size();
        String b = String.valueOf(a);
        try (FileWriter writer = new FileWriter("Lesson_1/"+ b + ".txt")) {
            for (String string : strings) {
                    writer.write(string + "\n");
            }
//            for (String string : strings) {
//                writer.write(string + "\n");
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
